This is a fun variation on fizz buzz, interweaving the traditional fizz buzz actions with animation.
A working copy can be viewed at http://fadetoproductions.com/ryan/fizzbuzzfun/
Created by Ryan von Kunes Newton